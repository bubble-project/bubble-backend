use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Bubble {
    id: usize,
    name: String,
    image: String,
}

impl Bubble {
    pub fn new(id: usize, name: String, image: String) -> Self {
        Self { id, name, image }
    }

    pub fn new_name(id: usize, name: String) -> Self {
        Self {
            id,
            name,
            image: "https://placehold.it/48x48".to_string(),
        }
    }
}
