use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Post {
    id: usize,
    title: String,
    image: Option<String>,
    headline: String,
}

impl Post {
    pub fn new(id: usize, title: String, image: Option<String>, headline: String) -> Self {
        Self {
            id,
            title,
            image,
            headline,
        }
    }
}
