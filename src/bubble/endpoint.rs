use crate::bubble::bubble::Bubble;
use crate::bubble::post::Post;
use actix_web::{get, web, HttpResponse, Result};

#[get("/user/bubbles")]
pub async fn my_bubbles() -> Result<HttpResponse> {
    let bubbles = vec![
        Bubble::new_name(1, "Tazz by eMAG".to_string()),
        Bubble::new_name(2, "Clanu Lejiunii".to_string()),
        Bubble::new_name(3, "Faiăr Silviu LIVE".to_string()),
        Bubble::new_name(4, "tineretposting".to_string()),
        Bubble::new_name(5, "BoJack Horseman Sadposting".to_string()),
        Bubble::new_name(
            6,
            "Eximperituserqethhzebibšiptugakkathšulweliarzaxułum".to_string(),
        ),
    ];

    Ok(HttpResponse::Ok().json(bubbles))
}

#[get("/bubble/{id}/posts")]
pub async fn posts_in_bubble(id: web::Path<usize>) -> Result<HttpResponse> {
    let mut posts = vec![];
    for id in 1..=10 {
        posts.push(Post::new(id, "This is the title".to_string(), None, "Lorem ipsum dolor, sit amet cons ectetur adipis icing elit. Praesen tium, quibusdam facere quo laborum maiores sequi nam tenetur laud.".to_string()));
    }

    Ok(HttpResponse::Ok().json(posts))
}
