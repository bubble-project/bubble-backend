mod bubble;

use actix_cors::Cors;
use actix_web::middleware::errhandlers::{ErrorHandlerResponse, ErrorHandlers};
use actix_web::{
    dev, get, http, http::header, middleware::Logger, web, App, HttpResponse, HttpServer, Result,
};

fn render_500<B>(mut res: dev::ServiceResponse<B>) -> Result<ErrorHandlerResponse<B>> {
    res.response_mut().headers_mut().insert(
        http::header::CONTENT_TYPE,
        http::HeaderValue::from_static("Error"),
    );
    Ok(ErrorHandlerResponse::Response(res))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix=info");
    env_logger::init();

    HttpServer::new(move || {
        App::new()
            .wrap(ErrorHandlers::new().handler(http::StatusCode::INTERNAL_SERVER_ERROR, render_500))
            .wrap(
                Cors::default()
                    .max_age(3600)
                    .supports_credentials()
                    .allow_any_origin()
                    .allow_any_header()
                    .allow_any_method(),
            )
            .wrap(Logger::default())
            .service(bubble::endpoint::my_bubbles)
            .service(bubble::endpoint::posts_in_bubble)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
